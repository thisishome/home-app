'use strict'

import { AppRegistry } from 'react-native'
import AppProvider from './build'

AppRegistry.registerComponent('ReactNativeTS', () => AppProvider)
