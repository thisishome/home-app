import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { decrementCounter, incrementCounter } from 'actions/CounterActions';
import Counter from 'components/counter/Counter';
import { AppState } from 'reducers/indexReducer';

export default connect(
  (state: AppState) => ({
    counterValue: state.counterState.counter,
  }),
  (dispatch: any) => ({
    incrementCounter: bindActionCreators(incrementCounter, dispatch),
    decrementCounter: bindActionCreators(decrementCounter, dispatch),
  }))(Counter);
