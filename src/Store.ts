import { createStore } from 'redux';
import indexReducer from 'reducers/indexReducer';

export default createStore(indexReducer);