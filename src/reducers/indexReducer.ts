import { combineReducers } from 'redux';
import { counterState, CounterState } from 'reducers/counterReducer';

export interface AppState { counterState: CounterState; }
export default combineReducers({counterState});