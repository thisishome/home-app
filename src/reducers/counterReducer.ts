import { Action } from 'redux';
import { DECREMENT_COUNTER, INCREMENT_COUNTER } from 'actions/CounterActions';

export interface CounterState { counter: number; }

const initialState: CounterState = { counter: 0 };

export function counterState(state: CounterState = initialState, action: Action): CounterState {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter + 1
      };
    case DECREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter - 1
      };
    default:
      return state;
  }
}
