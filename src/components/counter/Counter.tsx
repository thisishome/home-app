import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, TextStyle, ViewStyle } from 'react-native';
import HomeButton from 'components/homeButton/HomeButton';

interface Props {
  counterValue: number;
  incrementCounter(): any;
  decrementCounter(): any;
}
interface State {}

const styles = StyleSheet.create({
  counterText: {
    fontSize: 100,
    marginTop: 11,
    textAlign: 'center',
  } as TextStyle,
  button: {
    margin: 28,
  } as ViewStyle,
  unusedStyle: { // TODO: find a way to lint unused styles
    fontSize: 100,
  } as ViewStyle,
});

export default class Counter extends PureComponent<Props, State> {
  render() {
    return (
      <View>
        <Text style={styles.counterText}>{this.props.counterValue}</Text>
        <HomeButton onPress={this.props.incrementCounter} style={styles.button} label='+1'/>
        <HomeButton onPress={this.props.decrementCounter} style={styles.button} label='-1'/>
      </View>
    );
  }
}