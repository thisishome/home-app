import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, ViewStyle, TextStyle } from 'react-native';

interface Props {
  label: string;
  onPress(): any; // FIXME: Whats the correct typing here?
  style?: ViewStyle;
}
interface State {}

const styles = StyleSheet.create({
  buttonText: {
    color: '#ffffff',
    fontSize: 22,
    lineHeight: 44,
    textAlign: 'center',
  } as TextStyle,
  container:  {
    backgroundColor: '#83c736',
    borderRadius: 4,
    height: 50,
    padding: 4,
  } as ViewStyle,
});

export default class HomeButton extends Component<Props, State> {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={[styles.container, this.props.style]}>
        <Text style={styles.buttonText}>{this.props.label}</Text>
      </TouchableOpacity>
    );
  }
}