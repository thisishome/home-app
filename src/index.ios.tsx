import React from 'react';
import { Provider } from 'react-redux';
import CounterContainer from './containers/counter/CounterContainer';
import store from './Store';

export default function() {
  return (
    <Provider store={store}>
      <CounterContainer />
    </Provider>
  );
}
